package com.kayosystem.androiod.cloudtodolist.model;

import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;

/**
 * TodoListItemのViewModel.
 * Created by iwasaki_ma on 2017/09/25.
 */

public class TodoListViewModel extends ViewModel {
    private ArrayList<TodoItem> mItems = new ArrayList<>();
    private long mSelectedId;

    public TodoListViewModel() {
    }

    /**
     * TodoItemsを取得.
     *
     * @return
     */
    public ArrayList<TodoItem> getTodoItems() {
        return mItems;
    }

    /**
     * 選択中のアイテムIDを取得.
     *
     * @return
     */
    public long getSelectedId() {
        return mSelectedId;
    }

    public void setSelectedId(long selectedId) {
        mSelectedId = selectedId;
    }

    /**
     * リストに追加.
     *
     * @param item
     */
    public void addItem(TodoItem item) {
        mItems.add(item);
    }

    /**
     * リストから削除.
     *
     * @param item
     */
    public void removeItem(TodoItem item) {
        mItems.remove(item);
    }

    /**
     * IDでアイテムを検索.
     */
    public TodoItem findItemById(long id) {
        for (TodoItem item : mItems) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
}