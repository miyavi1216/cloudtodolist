package com.kayosystem.androiod.cloudtodolist.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kayosystem.androiod.cloudtodolist.R;
import com.kayosystem.androiod.cloudtodolist.databinding.ItemTodoListRowBinding;
import com.kayosystem.androiod.cloudtodolist.model.TodoItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Todoリストのアイテムを表示するアダプター.
 * Created by iwasaki_ma on 2017/08/24.
 */
public class TodoListAdapter extends RecyclerView.Adapter<TodoListAdapter.TodoViewHolder> {
    private ArrayList<TodoItem> mItems = new ArrayList<>();
    private Context mContext;
    private OnItemClickListener mListener;

    public TodoListAdapter(Context context, ArrayList<TodoItem> items) {
        mContext = context;
        mItems = items;
    }

    @Override
    public TodoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_todo_list_row, parent, false);
        return new TodoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TodoViewHolder holder, int position) {
        //リストアイテムを取得
        final TodoItem item = mItems.get(position);

        //リストアイテムをセット
        holder.getBinding().setItem(item);
        holder.getBinding().executePendingBindings();

        //リスナーをセット
        holder.getBinding().itemContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onItemClick(item);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    /**
     * 対象のRecyclerViewにSwipeとソートジェスチャーを実装.
     *
     * @param recyclerView
     */
    public void setItemGesture(RecyclerView recyclerView) {
        ItemTouchHelper itemDecor = new ItemTouchHelper(
                new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN,
                        ItemTouchHelper.LEFT) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                          RecyclerView.ViewHolder target) {
                        int from = viewHolder.getAdapterPosition();
                        int to = target.getAdapterPosition();
                        TodoItem tmp = mItems.remove(from);
                        mItems.add(to, tmp);
                        notifyItemMoved(from, to);
                        if (mListener != null) {
                            mListener.onItemMoved();
                        }
                        return true;
                    }

                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                        final int fromPos = viewHolder.getAdapterPosition();
                        notifyItemRemoved(fromPos);
                        if (mListener != null) {
                            mListener.onItemRemoved(mItems.get(fromPos));
                        }
                    }
                });
        itemDecor.attachToRecyclerView(recyclerView);
    }

    /**
     * クリックリスナーをセット.
     *
     * @param listener
     */
    public void setOnItemActionListener(OnItemClickListener listener) {
        mListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(TodoItem item);

        void onItemMoved();

        void onItemRemoved(TodoItem item);
    }

    /**
     * ViewHolder.
     */
    class TodoViewHolder extends RecyclerView.ViewHolder {
        ItemTodoListRowBinding binding;

        public TodoViewHolder(View v) {
            super(v);
            binding = DataBindingUtil.bind(v);
        }

        public ItemTodoListRowBinding getBinding() {
            return binding;
        }
    }
}