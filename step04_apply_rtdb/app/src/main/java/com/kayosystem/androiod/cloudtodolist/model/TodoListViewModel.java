package com.kayosystem.androiod.cloudtodolist.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.kayosystem.androiod.cloudtodolist.dao.RTDBDao;

import java.util.ArrayList;

/**
 * TodoListItemのViewModel.
 * Created by iwasaki_ma on 2017/09/25.
 */

public class TodoListViewModel extends ViewModel {
    private LiveData<ArrayList<TodoItem>> mItems = new MutableLiveData<>();
    private RTDBDao mFirebaseRDBDao;
    private long mSelectedId;

    public TodoListViewModel() {
        mFirebaseRDBDao = new RTDBDao();
        mItems = mFirebaseRDBDao.sync();
    }

    /**
     * TodoItemsを取得.
     *
     * @return
     */
    public LiveData<ArrayList<TodoItem>> getTodoItems() {
        return mItems;
    }

    /**
     * 選択中のアイテムIDを取得.
     *
     * @return
     */
    public long getSelectedId() {
        return mSelectedId;
    }

    public void setSelectedId(long selectedId) {
        mSelectedId = selectedId;
    }

    /**
     * リストに追加.
     *
     * @param item
     */
    public void addItem(TodoItem item) {
        ArrayList<TodoItem> items = mItems.getValue();
        items.add(item);
        save();
    }

    /**
     * リストから削除.
     *
     * @param item
     */
    public void removeItem(TodoItem item) {
        ArrayList<TodoItem> items = mItems.getValue();
        //削除対象のアイテムにnullにする(RTDBでの削除方法)
        int index = items.indexOf(item);
        items.remove(index);
        items.add(index, null);
        save();
    }

    /**
     * アイテムを更新.
     */
    public void save() {
        mFirebaseRDBDao.save();
    }

    /**
     * IDでアイテムを検索.
     */
    public TodoItem findItemById(long id) {
        for (TodoItem item : mItems.getValue()) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
}