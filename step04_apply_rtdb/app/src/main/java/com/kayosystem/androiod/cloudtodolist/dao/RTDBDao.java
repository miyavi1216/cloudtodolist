package com.kayosystem.androiod.cloudtodolist.dao;

import android.arch.lifecycle.MutableLiveData;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kayosystem.androiod.cloudtodolist.model.TodoItem;

import java.util.ArrayList;

/**
 * Firebase Realtime DatabaseへのDAO.
 * Created by masaya.iwasaki on 2017/09/28.
 */

public class RTDBDao {
    private MutableLiveData<ArrayList<TodoItem>> mTodoItems = new MutableLiveData<>();
    private FirebaseDatabase mDatabase;

    public RTDBDao() {
        init();
    }

    /**
     * 初期化処理.
     */
    public void init() {
        mDatabase = FirebaseDatabase.getInstance();
        mTodoItems.setValue(new ArrayList<TodoItem>());
    }

    /**
     * RTDBを同期
     */
    public MutableLiveData<ArrayList<TodoItem>> sync() {
        //FirebaseRDBから読み込む
        DatabaseReference rootRef = mDatabase.getReference("todo-item");
        rootRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<TodoItem> items = mTodoItems.getValue();
                items.clear();
                //Firebaseからアイテムを再生成
                Iterable<DataSnapshot> iterator = dataSnapshot.getChildren();
                while (iterator.iterator().hasNext()) {
                    DataSnapshot childDss = iterator.iterator().next();
                    TodoItem item = childDss.getValue(TodoItem.class);
                    items.add(item);
                }
                mTodoItems.setValue(items);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        return mTodoItems;
    }

    /**
     * RTDBへ保存.
     */
    public void save() {
        //FirebaseRDBへ保存
        DatabaseReference rootRef = mDatabase.getReference("todo-item");
        rootRef.setValue(mTodoItems.getValue());
    }
}
