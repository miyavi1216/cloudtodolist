package com.kayosystem.androiod.cloudtodolist.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kayosystem.androiod.cloudtodolist.R;
import com.kayosystem.androiod.cloudtodolist.databinding.FragmentTodoEditBinding;
import com.kayosystem.androiod.cloudtodolist.model.TodoItem;
import com.kayosystem.androiod.cloudtodolist.model.TodoListViewModel;

/**
 * Created by iwasaki_ma on 2017/08/28.
 */

public class TodoEditFragment extends Fragment {
    public static final String TAG = TodoEditFragment.class.getSimpleName();
    private static final String KEY_ITEM = "key-mEditItem";
    private FragmentTodoEditBinding mBinding;
    private TodoListViewModel mTodoListViewModel;
    private TodoItem mEditItem;

    public static TodoEditFragment newInstance() {
        Bundle args = new Bundle();
        TodoEditFragment fragment = new TodoEditFragment();
        //args.putLong(KEY_ITEM, id);
        //fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //ViewModelを生成
        mTodoListViewModel = ViewModelProviders.of(getActivity()).get(TodoListViewModel.class);

        //アイテムを取得
        //long id = getArguments().getLong(KEY_ITEM, -1);
        long id = mTodoListViewModel.getSelectedId();
        if (id == 0) {
            mEditItem = new TodoItem();
        } else {
            mEditItem = mTodoListViewModel.findItemById(id);
        }
        return inflater.inflate(R.layout.fragment_todo_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = DataBindingUtil.bind(view);

        if (mEditItem != null) {
            mBinding.title.setText(mEditItem.getTitle());
            mBinding.contents.setText(mEditItem.getContents());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //変更したデータを渡す
        String title = mBinding.title.getText().toString();
        String contents = mBinding.contents.getText().toString();
        if (TextUtils.isEmpty(title) && TextUtils.isEmpty(contents)) {
            //タイトルも内容もなければ削除とみなして消す
            mTodoListViewModel.removeItem(mEditItem);
        } else {
            //何らかの変更があれば更新対象
            mEditItem.setTitle(title);
            mEditItem.setContents(contents);
            mEditItem.setCreatedBy(System.currentTimeMillis());
            if (mEditItem.getId() == 0) {
                //新規作成
                mEditItem.setId(System.currentTimeMillis());
                mTodoListViewModel.addItem(mEditItem);
            } else {
                //編集
                mTodoListViewModel.save();
            }
        }
    }
}