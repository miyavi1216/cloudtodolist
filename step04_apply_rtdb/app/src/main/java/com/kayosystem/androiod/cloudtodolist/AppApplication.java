package com.kayosystem.androiod.cloudtodolist;

import android.app.Application;

import io.paperdb.Paper;

/**
 * Created by iwasaki_ma on 2017/09/12.
 */

public class AppApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Paper.init(getApplicationContext());
    }
}
