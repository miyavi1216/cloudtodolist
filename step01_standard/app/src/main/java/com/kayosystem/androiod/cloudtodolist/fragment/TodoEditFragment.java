package com.kayosystem.androiod.cloudtodolist.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kayosystem.androiod.cloudtodolist.R;
import com.kayosystem.androiod.cloudtodolist.databinding.FragmentTodoEditBinding;
import com.kayosystem.androiod.cloudtodolist.model.TodoItem;

/**
 * Created by iwasaki_ma on 2017/08/28.
 */

public class TodoEditFragment extends Fragment {
    public static final String TAG = TodoEditFragment.class.getSimpleName();
    private static final String KEY_ITEM = "key-item";
    private FragmentTodoEditBinding mBinding;
    private Listener mListener;
    private TodoItem mEditItem;

    public static TodoEditFragment newInstance(TodoItem item) {
        Bundle args = new Bundle();
        TodoEditFragment fragment = new TodoEditFragment();
        args.putParcelable(KEY_ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Listener) {
            mListener = (Listener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //データを取り出す
        mEditItem = getArguments().getParcelable(KEY_ITEM);
        if (mEditItem == null) {
            mEditItem = new TodoItem();
        }
        return inflater.inflate(R.layout.fragment_todo_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBinding = DataBindingUtil.bind(view);

        if (mEditItem != null) {
            mBinding.title.setText(mEditItem.getTitle());
            mBinding.contents.setText(mEditItem.getContents());
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //変更したデータを渡す
        String title = mBinding.title.getText().toString();
        String contents = mBinding.contents.getText().toString();
        mEditItem.setTitle(title);
        mEditItem.setContents(contents);
        mEditItem.setCreatedBy(System.currentTimeMillis());
        //Activityへ通知
        if (mListener != null) {
            mListener.onCloseSheet(mEditItem);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface Listener {
        void onCloseSheet(TodoItem item);
    }
}
