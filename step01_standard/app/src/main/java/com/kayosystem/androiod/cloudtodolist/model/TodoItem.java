package com.kayosystem.androiod.cloudtodolist.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by iwasaki_ma on 2017/08/28.
 */

public class TodoItem implements Parcelable {
    private long id;
    private String title;
    private String contents;
    private long createdBy;

    public TodoItem() {
        id = 0;
        title = "";
        contents = "";
        createdBy = 0;
    }

    public TodoItem(long id, String title, String contents, long createdBy) {
        this.id = id;
        this.title = title;
        this.contents = contents;
        this.createdBy = createdBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }

    protected TodoItem(Parcel in) {
        id = in.readLong();
        title = in.readString();
        contents = in.readString();
        createdBy = in.readLong();
    }

    /**
     * Parcelable.
     */
    public static final Creator<TodoItem> CREATOR = new Creator<TodoItem>() {
        @Override
        public TodoItem createFromParcel(Parcel in) {
            return new TodoItem(in);
        }

        @Override
        public TodoItem[] newArray(int size) {
            return new TodoItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(title);
        parcel.writeString(contents);
        parcel.writeLong(createdBy);
    }
}