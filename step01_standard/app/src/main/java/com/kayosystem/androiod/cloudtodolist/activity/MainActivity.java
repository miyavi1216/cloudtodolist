package com.kayosystem.androiod.cloudtodolist.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.kayosystem.androiod.cloudtodolist.R;
import com.kayosystem.androiod.cloudtodolist.adapter.TodoListAdapter;
import com.kayosystem.androiod.cloudtodolist.bean.LockedBottomSheetBehavior;
import com.kayosystem.androiod.cloudtodolist.databinding.ActivityMainBinding;
import com.kayosystem.androiod.cloudtodolist.fragment.TodoEditFragment;
import com.kayosystem.androiod.cloudtodolist.model.TodoItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements TodoEditFragment.Listener {
    private ActivityMainBinding mBinding;
    private LockedBottomSheetBehavior mBehavior;
    private ArrayList<TodoItem> mItems = new ArrayList<>();
    private TodoListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        //リストアイテムを作成
        mItems.add(new TodoItem(0, "牛乳を買う", "マックスバリュー", System.currentTimeMillis()));
        mItems.add(new TodoItem(1, "10時から釣り", "サビキ釣りで行く予定", System.currentTimeMillis()));
        mItems.add(new TodoItem(2, "ミーティングに出る", "プロジェクター準備しておく", System.currentTimeMillis()));

        //アダプタの初期化
        mAdapter = new TodoListAdapter(this, mItems);

        //RecyclerViewの初期化
        LinearLayoutManager llManger = new LinearLayoutManager(this);
        mBinding.todoList.setLayoutManager(llManger);
        DividerItemDecoration did = new DividerItemDecoration(this, llManger.getOrientation());
        mBinding.todoList.addItemDecoration(did);
        mBinding.todoList.setAdapter(mAdapter);
        mAdapter.setItemGesture(mBinding.todoList);
        mAdapter.setOnItemActionListener(new TodoListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(TodoItem item) {
                //編集画面を表示
                openEditScreen(item);
            }

            @Override
            public void onItemMoved() {
                //アイテムを入れ替え
            }

            @Override
            public void onItemRemoved(TodoItem item) {
                //アイテムを削除
                mItems.remove(item);
            }
        });

        //BottomSheetを追加
        mBehavior = (LockedBottomSheetBehavior) BottomSheetBehavior.from(mBinding.bottomSheet);
        mBehavior.setHideable(true);
        mBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        mBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    //閉じる時に編集画面を取り除く
                    TodoEditFragment f = (TodoEditFragment) getSupportFragmentManager().findFragmentByTag(TodoEditFragment.TAG);
                    if (f != null) {
                        getSupportFragmentManager().beginTransaction().remove(f).commit();
                    }
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        //新規作成ボタンをクリック
        mBinding.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openEditScreen(null);
            }
        });
    }

    @Override
    public void onBackPressed() {
        //ボトムシートが開いていたら閉じる
        if (mBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * 編集画面を開く.
     *
     * @param item
     */
    private void openEditScreen(TodoItem item) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.edit_container, TodoEditFragment.newInstance(item), TodoEditFragment.TAG)
                .commit();
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    /*===============================================
    ** TodoListFragment.Listener
    **================================================*/
    @Override
    public void onCloseSheet(TodoItem item) {
        if (TextUtils.isEmpty(item.getTitle()) && TextUtils.isEmpty(item.getContents())) {
            //タイトルも内容もなければ削除とみなして消す
            mItems.remove(item);
        } else {
            if (item.getId() == 0) {
                //新規作成
                item.setId(System.currentTimeMillis());
                mItems.add(item);
            }
        }
        mAdapter.notifyDataSetChanged();
    }
}