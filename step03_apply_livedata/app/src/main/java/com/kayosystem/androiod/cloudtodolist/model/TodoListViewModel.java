package com.kayosystem.androiod.cloudtodolist.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.ArrayList;

import io.paperdb.Paper;

/**
 * TodoListItemのViewModel.
 * Created by iwasaki_ma on 2017/09/25.
 */

public class TodoListViewModel extends ViewModel {
    private MutableLiveData<ArrayList<TodoItem>> mItems = new MutableLiveData<>();
    private long mSelectedId;

    public TodoListViewModel() {
        mItems.setValue(new ArrayList<TodoItem>());
        load();
    }

    /**
     * TodoItemsを取得.
     *
     * @return
     */
    public LiveData<ArrayList<TodoItem>> getTodoItems() {
        return mItems;
    }

    /**
     * 選択中のアイテムIDを取得.
     *
     * @return
     */
    public long getSelectedId() {
        return mSelectedId;
    }

    public void setSelectedId(long selectedId) {
        mSelectedId = selectedId;
    }

    /**
     * リストに追加.
     *
     * @param item
     */
    public void addItem(TodoItem item) {
        ArrayList<TodoItem> items = mItems.getValue();
        items.add(item);
        save();
    }

    /**
     * リストから削除.
     *
     * @param item
     */
    public void removeItem(TodoItem item) {
        ArrayList<TodoItem> items = mItems.getValue();
        mItems.getValue().remove(item);
        save();
    }

    public void load() {
        //読み込み
        ArrayList<TodoItem> items = Paper.book().read("todo-item", new ArrayList<TodoItem>());
        //更新
        mItems.setValue(items);
    }

    /**
     * アイテムを更新.
     */
    public void save() {
        //保存
        Paper.book().write("todo-item", mItems.getValue());
        //更新
        mItems.setValue(mItems.getValue());
    }

    /**
     * IDでアイテムを検索.
     */
    public TodoItem findItemById(long id) {
        for (TodoItem item : mItems.getValue()) {
            if (item.getId() == id) {
                return item;
            }
        }
        return null;
    }
}