package com.kayosystem.androiod.cloudtodolist.model;

/**
 * Created by iwasaki_ma on 2017/08/28.
 */

public class TodoItem {
    private long id;
    private String title;
    private String contents;
    private long createdBy;

    public TodoItem() {
        id = 0;
        title = "";
        contents = "";
        createdBy = 0;
    }

    public TodoItem(long id, String title, String contents, long createdBy) {
        this.id = id;
        this.title = title;
        this.contents = contents;
        this.createdBy = createdBy;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(long createdBy) {
        this.createdBy = createdBy;
    }
}